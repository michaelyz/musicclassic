package net.michaelzhou.android.music403;

class CircularBuffer {
    public static long[] expand(long[] orig, int start, int len, int cap) {
        long[] buf = new long[cap];
        
        for (int i = 0; i != len; ++i) {
            buf[i] = orig[start];
            
            start++;
            start %= orig.length;
        }
        
        return buf;
    }
}